import { Component, OnInit } from '@angular/core';
import { Salary } from "../entity/salary";

@Component({
  selector: 'app-form-salary',
  templateUrl: './form-salary.component.html',
  styleUrls: ['./form-salary.component.css']
})
export class FormSalaryComponent implements OnInit {

	salaryModel = new Salary("", "", undefined);
  constructor() { }

  ngOnInit(): void {
  }
  
  formularioEnviado(){
    /*
    Aquí el formulario ha sido enviado, ya sea
    por presionar el botón, presionar Enter, etcétera
    */
    console.log("Form Send and created: ", this.salaryModel)
    alert("Send");
  }

}
