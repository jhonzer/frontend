export class Salary {
  constructor(
        public Year: number,
        public Month: number,
		public Office: string,
		public EmployeeCode: string,
        public EmployeeName: string,
		public EmployeeSurname: string,
		public Division: string,
		public Position: string,
		public Grade: string,
		public BeginDate: date,
		public Birthday: date,
		public IdentificationNumber: string,
		public BaseSalary: decimal,
		public ProductionBonus: decimal,
		public CompensationBonus: decimal,
		public Commission: decimal,
		public Contributions: decimal
    ) { }
}